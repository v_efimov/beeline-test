import { createLessonCard } from "./lessonCard.js";

/**
 * Функция создания слайдера с уроками.
 * @param {*} lessons Уроки.
 * @param {*} type Опциональный аргумент, который указывает тип слайдера. Например: 'AllLectures' - создаст слайдер с заголовками для всех уроков.
 */
export function createLessonSlider(lessons, type) {
	const template = document.querySelector('#slider');
    const item = template.content.cloneNode(true);
    const fragment = document.createDocumentFragment();

	const parentNode = item.querySelector('.slider');

	if (type === 'AllLectures') {
		item.querySelector('.slider').id = 'all-lectures';
		item.querySelector('.slider__category-title').textContent = 'Все лекции на нашем сайте';
		item.querySelector('.slider__title').textContent = 'All lectures';
	} else {
		item.querySelector('.slider').id = `${lessons[0].uniqGroup}`;
		item.querySelector('.slider__category-title').textContent = lessons[0].level;
		item.querySelector('.slider__title').textContent = lessons[0].list;
	}

	item.querySelector('.slider__show-all-btn').textContent = `${lessons.length} ${declOfNum(lessons.length, ['лекция', 'лекции', 'лекций'])}`;

	//TODO В отдельынй метод
	let allKeys = [];
	lessons.forEach((el) => {
		allKeys.push(el.group);
	});
	let uniqueKeys = [...new Set(allKeys)].reverse();

	uniqueKeys.forEach((el) => {
		createSliderButton(el, item.querySelector('.slider__buttons'));
	})

	fragment.appendChild(item);
    document.querySelector('.content').appendChild(fragment);

    lessons.forEach((lesson) => {
		createLessonCard(lesson, parentNode.querySelector('.slider__items'));
    });
}

/**
 * Обработчик событий слайдера.
 * @param {*} slider - Слайдер, в котором произошло событие.
 * @param {*} eventNode - Элемент слайдера, на котором произошло событие.
 * @returns 
 */
export function sliderHandle(slider, eventNode) {
	const itemsNode = slider.querySelector('.slider__items');
	const leftBtn = slider.querySelector('.slider__left-btn');
	const rightBtn = slider.querySelector('.slider_right-btn');
	let allItems = slider.querySelectorAll('.item');
	let firstItem;
	let lastItem;

	//Обработка закрытия всех лекций.
	if(eventNode.classList.contains('slider__show-all-btn') && itemsNode.classList.contains('slider__items_showed-all')) {
		enablePagination();
		itemsNode.classList.remove('slider__items_showed-all');
		eventNode.textContent = `${getCountDisplayedItems()} ${declOfNum(getCountDisplayedItems(), ['лекция', 'лекции', 'лекций'])}`;
		return;
	}

	//Обработка открытия всех лекций.
	if(eventNode.classList.contains('slider__show-all-btn')) {
		disablePagination();
		itemsNode.classList.add('slider__items_showed-all');
		eventNode.textContent = 'Скрыть';
		return;
	}

	//Обработка стрелки влево.
	if(eventNode.classList.contains('slider__left-btn')) {
		checkEnvironment();
		itemsNode.appendChild(firstItem);
	}

	//Обработка стрелки вправо.
	if(eventNode.classList.contains('slider_right-btn')) {
		checkEnvironment();
		itemsNode.insertBefore(lastItem, firstItem);
	}

	//Обработка фильтрации по группам.
	if(eventNode.classList.contains('slider__category-btn')) {
		if (eventNode.classList.contains('button_active')) {
			resetFilter();
			eventNode.classList.remove('button_active');
		} else {
			resetFilter();
			slider.querySelectorAll('.slider__category-btn').forEach((btn) => 
				btn.classList.remove('button_active')
			);
			eventNode.classList.add('button_active');
			allItems.forEach((item) => {
				if( item.getAttribute('data-group') !== eventNode.innerHTML) {
					item.classList.add('item_hidden');
				}
				slider.querySelector('.slider__show-all-btn').textContent = `${getCountDisplayedItems()} ${declOfNum(getCountDisplayedItems(), ['лекция', 'лекции', 'лекций'])}`;
			})
		}
	}

	/**
	 * Проверяет с каким списом элементов, на момент вызова, происходят действия.
	 */
	function checkEnvironment() {
		const allShowedItems = Array.from(slider.querySelectorAll('.item')).filter((item) => !item.classList.contains('item_hidden'));
		firstItem = allShowedItems[0];
		lastItem = allShowedItems[allShowedItems.length - 1]; 
	}

	/**
	 * Отключает пагинацию.
	 */
	function disablePagination() {
		leftBtn.setAttribute('disabled', 'disabled');
		rightBtn.setAttribute('disabled', 'disabled');
		rightBtn.classList.add('button_disabled');
		leftBtn.classList.add('button_disabled');
	}

	/**
	 * Включает пагинацию.
	 */
	function enablePagination() {
		leftBtn.removeAttribute('disabled');
		rightBtn.removeAttribute('disabled');
		rightBtn.classList.remove('button_disabled');
		leftBtn.classList.remove('button_disabled');
	}

	/**
	 * Сбрасывает фильтр.
	 */
	function resetFilter() {
		allItems.forEach((item) => item.classList.remove('item_hidden'));
		slider.querySelector('.slider__show-all-btn').textContent = `${getCountDisplayedItems()} лекций`;
	}

	/**
	 * Считает сколько активных элементов в слайдере (активные = отображающиеся).
	 */
	function getCountDisplayedItems() {
		let count = 0;
		allItems.forEach((item) => {
			if(!item.classList.contains('item_hidden')) {
				count++;
			}
		});

		return count;
	}
}

/**
 * Создает кнопки групп для слайдера.
 * @param {*} text Заголовок кнопки.
 * @param {*} parent Блок с кнопками.
 */
function createSliderButton(text, parent) {
	const newButton = document.createElement('button');
	newButton.classList.add('slider__category-btn', 'button', 'button_primary');
	newButton.textContent = text;
	parent.insertBefore(newButton, parent.firstChild);
}

/**
 * Склоняет существительные в зависимости от числа.
 * @param {*} number Какое число просклонять.
 * @param {*} words Словарь со словами.
 * @returns Существительное в правильном склонении.
 */
function declOfNum(number, words) {  
    return words[(number % 100 > 4 && number % 100 < 20) ? 2 : [2, 0, 1, 1, 1, 2][(number % 10 < 5) ? Math.abs(number) % 10 : 5]];
}
