/**
 * Функция рендеринга карточки урока по шаблону.
 * @param {*} lesson Урок 
 * @param {*} parentNode Родительский элемент, в который происходит запись урока
 */
export function createLessonCard(lesson, parentNode) {
	const template = document.querySelector('#lesson');
    const fragment = document.createDocumentFragment();
    const item = template.content.cloneNode(true);
    
    item.querySelector('.item__image').src = lesson.image;
    item.querySelector('.item__type').textContent = lesson.typeLesson;
    item.querySelector('.item__title').textContent = lesson.name;
    item.querySelector('.item__sub-title').textContent = lesson.subscription;
    item.querySelector('.item__date').textContent = lesson.date;
    item.querySelector('.slider__item').setAttribute('data-group', lesson.group);

    fragment.appendChild(item);
    parentNode.appendChild(fragment);
}