export function createLessonList() {
    const lessons = document.querySelectorAll('#all-lectures .item');
    const lessonList = {};
  
    lessons.forEach(lesson => {
        const lessonGroup = lesson.getAttribute('data-group');
        const lessonTitle = lesson.querySelector('.item__title').textContent;
        const lessonDescription = lesson.querySelector('.item__sub-title').textContent;
        const lessonDate = lesson.querySelector('.item__date').textContent;
        const lessonImg = lesson.querySelector('.item__image').getAttribute('src');
        const lessonLabel = lesson.querySelector('.item__type').textContent;
  
        if (!lessonList[lessonGroup]) {
            lessonList[lessonGroup] = [];
        }

        lessonList[lessonGroup].push({
            "title" :       lessonTitle,
            "description" : lessonDescription,
            "date" :        lessonDate,
            "image" :       lessonImg,
            "label" :       lessonLabel,
        });
    });
  
    return lessonList;
}