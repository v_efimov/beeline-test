import { sliderHandle,  createLessonSlider } from './slider.js';
import { textToUpperCase, textOverflow } from './hw06.js';
import { accordionHandle  } from './accordion.js';
import { createLessonList } from './hw09.js';

import { getData } from '../api/getData.js';


let lectures = [];
getData("../api/db/lectures.json").then(
	(response) => {
		lectures = JSON.parse(response);
		createLessonSlider(lectures, 'AllLectures');
		
		let allKeys = [];	//TODO В отдельный метод
		lectures.forEach((el) => {
			allKeys.push(el.uniqGroup);
		});
		const uniqueKeys = [...new Set(allKeys)];
	
		for (let i = 0; i < uniqueKeys.length; i++) {
			createLessonSlider(lectures.filter(lesson => 
				lesson.uniqGroup === uniqueKeys[i]
			));
		}
		
    	const sliders = document.querySelectorAll('.slider');
  		sliders.forEach((slider) => {
    		slider.addEventListener("click", (event) => {
        		sliderHandle(slider, event.target)
    		});
		})

		console.log('LessonList', createLessonList());
		accordionHandle();
		textToUpperCase('base-lvl-html-css', 'item__title');
		textOverflow('base-lvl-html-css', 'item__sub-title', 20);
	},
  	(error) => alert(`Rejected: ${error}`)
);
