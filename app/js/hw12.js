// Выполнить задания и в комментариях пояснить, почему получили такой результат:

//1
const greetPerson = () => {
    const sayName = (name) => {
        return `hi, i am ${name}`
    }
    return sayName
}

const greeting = greetPerson();
console.log(greeting('Pavel'));
console.log(greeting('Irina'));
// Результат: 'hi, i am ${name}' и следом 'hi, i am Irina'
// Создали замыкание в переменной greeting и далее вызываем её с разным контекстом (именами).


//2
let y1 = 'test';
const foo1 = () => {
    var newItem = 'hello';
    console.log(y1);
}
foo1();
console.log(newItem);
// Результат: 'test' и следом Uncaught ReferenceError: newItem is not defined
// Переменная y будет взята из окружения выше, т.к. в области функции foo() она не определена;
// Переменная newItem не будет найдена, потому что она находится только в локальной области видимости.

//3
let y = 'test';
let test = 2;
const foo2 = () => {
    const test = 5;
    const bar = () => {
        console.log(5 + test);
    }
    bar()
}
foo2();
// Результат: 10
// Имеем 2 переменные test: одна глобальная, вторая локальная в функции foo();
// Эта переменная используется в функции bar(), в лексическом окружении которой её нет, следовательно она продолжит поиск на уровень выше и найдет её в ф-ции foo(), где она равна 5.

//4
const bar = () => {
    const b = 'no test'
}
bar();

const foo = (() => {
    console.log(b);
})();
const b = 'test';
// Результат: Uncaught ReferenceError: b is not defined
// Имеем 2 переменной b: одна локальная в ф-ции bar(), вторая глобальная;
// Будет ошибка потому, что foo() вызывается ДО инициализации глобальной переменной b, а доступа к локальной переменной из функции bar() у функции foo() нет.