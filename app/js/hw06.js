export function textToUpperCase(NodeID, textNode) {
    const elementsToChange = document.querySelectorAll(`#${NodeID} .${textNode}`);

    elementsToChange.forEach(element => {
       element.innerHTML = element.innerHTML.toUpperCase();
    });
}

export function textOverflow(NodeID, textNode, countOfChar) {
    const elementsToChange = document.querySelectorAll(`#${NodeID} .${textNode}`);

    elementsToChange.forEach(element => {
        if (element.innerHTML.length > countOfChar) {
            element.innerHTML = element.innerHTML.slice(0, countOfChar) + "...";
        }
    });
}
