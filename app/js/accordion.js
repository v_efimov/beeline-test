export function accordionHandle() {
    const accordions = document.querySelectorAll('.accordion');

    accordions.forEach((accordion) => {
        accordion.addEventListener('click', (event) => {
            event.target.parentElement.classList.toggle('accordion_opened');
        });
    });
}
